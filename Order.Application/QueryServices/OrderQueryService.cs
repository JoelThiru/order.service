﻿namespace Order.Application.QueryServices
{
    using AutoMapper;
    using Order.Application.Exceptions;
    using Order.Application.HttpServices;
    using Order.Application.QueryServices.Dtos;
    using Order.Domain.Repositories;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class OrderQueryService : IOrderQueryService
    {
        private readonly IOrderRepository orderRepository;
        private readonly ICustomerApiService customerApiService;
        private readonly IMapper mapper;

        public OrderQueryService(
            IOrderRepository orderRepository,
            ICustomerApiService customerApiService,
            IMapper mapper)
        {
            this.orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            this.customerApiService = customerApiService ?? throw new ArgumentNullException(nameof(customerApiService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<GerOrderReponseModel> GetOrder(GetOrderRequestModel model, CancellationToken cancellationToken)
        {
            var customer = await this.customerApiService.GetByEmailIdAsync(model.User, cancellationToken)
                .ConfigureAwait(false);

            if(customer is null)
            {
                return null;
            }

            if(!customer.CustomerId.ToLower().Equals(model.CustomerId.ToLower()))
            {
                throw new InvalidCustomerIdException("User's customerId does not match");
            }

            var customerDto = this.mapper.Map<QueryServices.Dtos.Customer>(customer);

            var order = await this.orderRepository.GetRecentOrderByCustomerIdAsync(model.CustomerId, cancellationToken)
                .ConfigureAwait(false);

            if(order is null)
            {
                return new GerOrderReponseModel
                {
                    Customer = customerDto
                };
            }

            var orderDto = this.mapper.Map<Order>(order);
            orderDto.DeliveryAddress = customer.DeliveryAddress;

            return new GerOrderReponseModel
            {
                Customer = customerDto,
                Order = orderDto
            };
        }
    }
}
