﻿namespace Order.Application.QueryServices.Dtos
{
    public class GerOrderReponseModel
    {
        public Customer Customer { get; set; }
        public Order Order { get; set; }
    }
}
