﻿namespace Order.Application.QueryServices.Dtos
{
    public class GetOrderRequestModel
    {
        public string CustomerId { get; set; }
        public string User { get; set; }
    }
}
