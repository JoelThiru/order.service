﻿using System;

namespace Order.Application.QueryServices.Dtos
{
    public class Order
    {
        public int OrderNumber { get; set; }
        public string OrderDate { get; set; }
        public string DeliveryExpected { get; set; }
        public string DeliveryAddress { get; set; }
        public OrderItem[]  OrderItems { get; set; }
    }
}
