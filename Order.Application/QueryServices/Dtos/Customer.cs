﻿namespace Order.Application.QueryServices.Dtos
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
