﻿namespace Order.Application.QueryServices.Dtos
{
    public class OrderItem
    {
        public string Product { get; set; }
        public int Quantity { get; set; }
        public decimal PriceEach { get; set; }
    }
}
