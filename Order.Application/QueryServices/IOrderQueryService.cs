﻿namespace Order.Application.QueryServices
{
    using Order.Application.QueryServices.Dtos;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IOrderQueryService
    {
        public Task<GerOrderReponseModel> GetOrder(GetOrderRequestModel model, CancellationToken cancellationToken);
    }
}
