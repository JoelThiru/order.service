﻿
namespace Order.Application
{
    using System;

    public static class Extensions
    {
        public static string DateString(this DateTime value)
        {
           return value.ToString("dd-MMM-yyyy");
        }
    }
}
