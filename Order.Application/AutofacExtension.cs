﻿
namespace Order.Application
{
    using Autofac;
    using AutoMapper;

    public static class AutofacExtension
    {
        public static void SetupAutoMapper(this ContainerBuilder builder, bool assertConfigIsValid, params Profile[] profiles)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }
            });

            if (assertConfigIsValid)
            {
                mapperConfig.AssertConfigurationIsValid();
            }

            _ = builder.RegisterInstance(mapperConfig).AsSelf().SingleInstance();

            _ = builder.Register(c =>
            {
                var context = c.Resolve<IComponentContext>();
                var config = context.Resolve<MapperConfiguration>();

                return config.CreateMapper(context.Resolve);
            }).As<IMapper>().SingleInstance();
        }
    }
}
