﻿namespace Order.Application.HttpServices
{
    public class CustomerApiSetting
    {
        public string Endpoint { get; set; }
        public string ApiKey { get; set; }
    }
}
