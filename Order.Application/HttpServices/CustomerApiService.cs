﻿namespace Order.Application.HttpServices
{
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using Order.Application.HttpServices.Dtos;
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    public class CustomerApiService : ICustomerApiService
    {
        private readonly CustomerApiSetting customerApiSetting;
        private readonly ILogger<CustomerApiService> logger;

        public CustomerApiService(
            CustomerApiSetting customerApiSetting,
            ILogger<CustomerApiService> logger)
        {
            this.customerApiSetting = customerApiSetting ?? throw new ArgumentNullException(nameof(customerApiSetting));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Customer> GetByEmailIdAsync(string userId, CancellationToken cancellationToken)
        {
            var client = new HttpClient();

            var response = await client.GetAsync($"{this.customerApiSetting.Endpoint}/GetUserDetails?code={this.customerApiSetting.ApiKey}&email={userId}")
                .ConfigureAwait(false);

            if(response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var stringContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<Customer>(stringContent);
            }

            if(response.StatusCode != System.Net.HttpStatusCode.NotFound)
            {
                this.logger.LogError($"Unexpected response from Customer Api, Status: {response.StatusCode}. {response.ReasonPhrase}");
            }

            return null;
        }
    }
}
