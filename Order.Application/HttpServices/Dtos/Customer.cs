﻿namespace Order.Application.HttpServices.Dtos
{
    using System;

    public class Customer
    {
        public string Email { get; set; }
        public string CustomerId { get; set; }
        public bool WebSite { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime LastLaggedIn { get; set; }
        public string HouseNumber { get;  set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string PreferredLanguage { get; set; }
        public string DeliveryAddress =>
            $"{this.HouseNumber} {this.Street}, {this.Town}, {this.Postcode}";
    }
}
