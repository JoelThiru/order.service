﻿namespace Order.Application.HttpServices
{
    using Order.Application.HttpServices.Dtos;
    using System.Threading;
    using System.Threading.Tasks;

    public interface ICustomerApiService
    {
        public Task<Customer> GetByEmailIdAsync(string userId, CancellationToken cancellationToken);
    }
}
