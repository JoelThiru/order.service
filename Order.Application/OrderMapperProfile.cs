﻿namespace Order.Application
{
    using AutoMapper;

    public class OrderMapperProfile : Profile
    {
        public OrderMapperProfile() : base()
        {
            this.CreateMap<HttpServices.Dtos.Customer, QueryServices.Dtos.Customer>();
            this.CreateMap<Order.Domain.Order, QueryServices.Dtos.Order>()
                 .ForMember(x => x.DeliveryAddress, opt => opt.Ignore())
                 .ForMember(x => x.OrderNumber, opt => opt.MapFrom(y => y.OrderId))
                 .ForMember(x => x.OrderDate, opt => opt.MapFrom(y => y.OrderDate.DateString()))
                 .ForMember(x => x.DeliveryExpected, opt => opt.MapFrom(y => y.DeliveryExpected.DateString()));
            this.CreateMap<Order.Domain.OrderItem, QueryServices.Dtos.OrderItem>()
                .ForMember(x => x.Product, opt => opt.MapFrom(y => y.ProductName()))
                .ForMember(x => x.PriceEach, opt => opt.MapFrom(y => y.Price));
        }
    }
}
