﻿
namespace Order.Application.Exceptions
{
    using System;

    public class InvalidCustomerIdException : Exception
    {
        public InvalidCustomerIdException()
        {
        }

        public InvalidCustomerIdException(string message) : base(message)
        {
        }
    }
}
