﻿
namespace Order.Application
{
    using Autofac;
    using AutoMapper;
    using Microsoft.Extensions.Configuration;
    using Order.Application.HttpServices;
    using Order.Persistence;
    using System;
    using System.Reflection;

    public class OrderApplicationModule : Autofac.Module
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="SquadApplicationModule"/> class.
        /// </summary>
        /// <param name="configuration">The configuration <see cref="IConfiguration"/>.</param>
        public OrderApplicationModule(IConfiguration configuration)
            => this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

        /// <summary>
        /// The Load.
        /// </summary>
        /// <param name="builder">The builder <see cref="ContainerBuilder"/>.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsImplementedInterfaces();

            builder.RegisterModule(new OrderPersistenceModule(this.configuration));

            var appConfig = this.configuration
            .GetSection("CustomerApiSetting")
            .Get<CustomerApiSetting>();

            builder.RegisterInstance(appConfig)
            .As<CustomerApiSetting>()
            .SingleInstance();

            builder.SetupAutoMapper(assertConfigIsValid: true, new Profile[] { new OrderMapperProfile() });
        }
    }
}
