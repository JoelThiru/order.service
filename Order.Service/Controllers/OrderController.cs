﻿namespace Order.Service.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Order.Application.Exceptions;
    using Order.Application.QueryServices;
    using Order.Application.QueryServices.Dtos;
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;

    [ApiController]
    [Route("[controller]")]
    public class OrderController : Controller
    {
        private readonly ILogger<OrderController> logger;
        private readonly IOrderQueryService orderQueryService;

        public OrderController(
            IOrderQueryService orderQueryService, 
            ILogger<OrderController> logger)
        {
            this.orderQueryService = orderQueryService ?? throw new ArgumentNullException(nameof(orderQueryService));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        public async Task<IActionResult> GetOrderForCustomer(GetOrderRequestModel model, CancellationToken cancellationToken)
        {
            return await this.WithTryCatchLogging(
               async () =>
               {
                   var order = await this.orderQueryService.GetOrder(model, cancellationToken)
                    .ConfigureAwait(false);

                   if (order is null)
                   {
                       return this.NotFound();
                   }

                   return this.Ok(order);
               }).ConfigureAwait(false);
        }

        private async Task<IActionResult> WithTryCatchLogging(Func<Task<IActionResult>> asyncFunc, [CallerMemberName] string method = null)
        {
            try
            {
                return await asyncFunc().ConfigureAwait(false);
            }
            catch (InvalidCustomerIdException ex)
            {
                return this.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, "ERROR Processing request: {method}", method);

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Unable to process request");
            }
        }
    }
}
