﻿namespace Order.Service
{
    using Autofac;
    using Microsoft.Extensions.Configuration;
    using Order.Application;
    using System;
    using System.Reflection;

    public class OrderServiceModule : Autofac.Module
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderServiceModule"/> class.
        /// </summary>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/>.</param>
        public OrderServiceModule(IConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        /// <summary>
        /// The Load.
        /// </summary>
        /// <param name="builder">The builder<see cref="ContainerBuilder"/>.</param>
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsImplementedInterfaces();

            builder.RegisterModule(new OrderApplicationModule(this.configuration));
        }
    }
}
