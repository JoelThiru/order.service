﻿
namespace Order.Domain.Tests
{
    using FluentAssertions;
    using FluentAssertions.Execution;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Xunit;

    public class OrderItemTests
    {
        [Theory]
        [ClassData(typeof(OrderItemTestData))]
        public void ProductNameIsGiftIfOrderContainsGift(Domain.OrderItem orderItem, string productName)
        {
            using (new AssertionScope())
            {
                orderItem.ProductName().Should().Be(productName);
            }
        }

        public class OrderItemTestData : IEnumerable<object[]>
        {
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return new object[]
                {
                    new OrderItem
                    {
                        OrderItemId = 1,
                        Price = 12.3m,
                        Quantity = 1,
                        Returnable = true,
                        Product = new Product
                        {
                            ProductName = "Skateboard"
                        },
                        Order = new Domain.Order
                        {
                            OrderId = 1,
                            CustomerId = "1234",
                            OrderDate = DateTime.Today.AddDays(-5),
                            DeliveryExpected = DateTime.Today.AddDays(-2),
                            ContainsGift = false,
                            ShippingMode = "shippingMode",
                        } 
                    },
                    "Skateboard"
                };
                yield return new object[]
                {
                    new OrderItem
                    {
                        OrderItemId = 1,
                        Price = 12.3m,
                        Quantity = 1,
                        Returnable = true,
                        Product = new Product
                        {
                            ProductName = "Chocolate"
                        },
                        Order = new Domain.Order
                        {
                            OrderId = 1,
                            CustomerId = "1234",
                            OrderDate = DateTime.Today.AddDays(-5),
                            DeliveryExpected = DateTime.Today.AddDays(-2),
                            ContainsGift = true,
                            ShippingMode = "shippingMode",
                        }
                    },
                    "Gift"
                };
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                throw new NotImplementedException();
            }
        }
    }
}
