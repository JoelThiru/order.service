﻿namespace Order.Service.Tests.Controllers
{
    using AutoFixture;
    using AutoFixture.AutoMoq;
    using FluentAssertions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Order.Application.Exceptions;
    using Order.Application.QueryServices;
    using Order.Application.QueryServices.Dtos;
    using Order.Service.Controllers;
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public class OrderControllerTests
    {
        [Fact]
        public void Constructor_GaurdsAgainstNullArguments()
        {
            TestUtils.AssertConstructorsGuardAgainstNullArgs<OrderController>();
        }

        [Fact]
        public async Task GetOrderForCustomer_ReturnsExpectedReponse()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "user@valid.com",
                CustomerId = "ValidCustomer"
            };
            var stubResponseModel = new GerOrderReponseModel
            {
                Customer = new Customer
                {
                    FirstName = "firstname",
                    LastName = "lastname"
                },
                Order = new Order
                {
                    DeliveryAddress = "deliveryAddress",
                    DeliveryExpected = "02-May-1919",
                    OrderDate = "01-May-1919",
                    OrderNumber = 4567,
                    OrderItems = new OrderItem[]
                    {
                        new OrderItem { Product = "Product 1", PriceEach = 23.4m, Quantity = 1 }
                    }
                }
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockOrderQueryService.Setup(x => x.GetOrder(stubRequestModel, default))
                .ReturnsAsync(stubResponseModel);

            var result = await sut.GetOrderForCustomer(stubRequestModel, default).ConfigureAwait(false) as OkObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(200);
            result.Value.Should().Be(stubResponseModel);
        }

        [Fact]
        public async Task GetOrderForCustomer_ReturnsNotFound()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "user@valid.com",
                CustomerId = "ValidCustomer"
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockOrderQueryService.Setup(x => x.GetOrder(stubRequestModel, default))
                .ReturnsAsync((GerOrderReponseModel)null);

            var result = await sut.GetOrderForCustomer(stubRequestModel, default).ConfigureAwait(false) as NotFoundResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(404);
        }

        [Fact]
        public async Task GetOrderForCustomer_ReturnsBadRequest()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "user@valid.com",
                CustomerId = "ValidCustomer"
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockOrderQueryService.Setup(x => x.GetOrder(stubRequestModel, default))
                .ThrowsAsync(new InvalidCustomerIdException("some error"));

            var result = await sut.GetOrderForCustomer(stubRequestModel, default).ConfigureAwait(false) as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(400);
            result.Value.Should().Be("some error");
        }

        [Fact]
        public async Task GetOrderForCustomer_ReturnsInternalServerError()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "user@valid.com",
                CustomerId = "ValidCustomer"
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockOrderQueryService.Setup(x => x.GetOrder(stubRequestModel, default))
                .ThrowsAsync(new Exception("some error"));

            var result = await sut.GetOrderForCustomer(stubRequestModel, default).ConfigureAwait(false) as ObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be(500);
        }


        private class SutBuilder
        {
            private readonly Fixture fixture;

            public Mock<ILogger<OrderController>> MockLogger { get; }
            public Mock<IOrderQueryService> MockOrderQueryService { get; }

            public SutBuilder()
            {
                this.fixture = new Fixture();
                this.fixture.Customize(new AutoMoqCustomization());

                this.MockLogger = this.fixture.Freeze<Mock<ILogger<OrderController>>>();
                this.MockOrderQueryService = this.fixture.Freeze<Mock<IOrderQueryService>>();
            }

            public OrderController Build => new OrderController(this.MockOrderQueryService.Object, this.MockLogger.Object);
        }
    }
}
