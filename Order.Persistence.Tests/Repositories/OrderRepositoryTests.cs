﻿namespace Order.Persistence.Tests.Repositories
{
    using FluentAssertions;
    using Order.Application.Tests;
    using Order.Domain;
    using Order.Persistence.Repositories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class OrderRepositoryTests
    {
        [Fact]
        public void Constructor_GuardsAgainstNullArgs()
        {
            TestUtils.AssertConstructorsGuardAgainstNullArgs<OrderRepository>();
        }

        [Fact]
        public async Task GetRecentOrderByCustomerIdAsyncReturnsOrderWithIncludes()
        {
            var stubProduct = new Domain.Product
            {
                ProductId = 1,
                PackHeight = 12.3m,
                PackWeight = 12.3m,
                PackWidth = 12.3m,
                Colour = "orange",
                ProductName = "Teddy bear",
                Size = "small"
            };
            var stubProduct1 = new Domain.Product
            {
                ProductId = 2,
                PackHeight = 12.3m,
                PackWeight = 12.3m,
                PackWidth = 12.3m,
                Colour = "orange",
                ProductName = "Skateboard",
                Size = "small"
            };
            var stubProduct2 = new Domain.Product
            {
                ProductId = 3,
                PackHeight = 12.3m,
                PackWeight = 12.3m,
                PackWidth = 12.3m,
                Colour = "orange",
                ProductName = "Remote controller",
                Size = "small"
            };
            var stubOrder = new Domain.Order
            {
                OrderId = 1,
                CustomerId = "1234",
                OrderDate = DateTime.Today.AddDays(-5),
                DeliveryExpected = DateTime.Today.AddDays(-2),
                ContainsGift = false,
                ShippingMode = "shippingMode",
                OrderItems = new List<OrderItem>
                {
                    new OrderItem
                    {
                        OrderItemId = 1,
                        Price = 12.3m,
                        Quantity = 1,
                        Returnable = true,
                        ProductId = 1
                    }
                }
            };
            var stubOrder2 = new Domain.Order
            {
                OrderId = 2,
                CustomerId = "12334",
                OrderDate = DateTime.Today.AddDays(-4),
                DeliveryExpected = DateTime.Today.AddDays(-2),
                ContainsGift = false,
                ShippingMode = "shippingMode",
                OrderItems = new List<OrderItem>
                {
                    new OrderItem
                    {
                        OrderItemId = 2,
                        Price = 12.3m,
                        Quantity = 1,
                        Returnable = true,
                        ProductId = 2
                    }
                }
            };
            var stubOrder3 = new Domain.Order
            {
                OrderId = 3,
                CustomerId = "1234",
                OrderDate = DateTime.Today.AddDays(-3),
                DeliveryExpected = DateTime.Today.AddDays(-2),
                ContainsGift = false,
                ShippingMode = "shippingMode",
                OrderItems = new List<OrderItem>
                {
                    new OrderItem
                    {
                        OrderItemId = 3,
                        Price = 12.3m,
                        Quantity = 1,
                        Returnable = true,
                        ProductId = 3
                    },
                    new OrderItem
                    {
                        OrderItemId = 4,
                        Price = 12.3m,
                        Quantity = 1,
                        Returnable = true,
                        ProductId = 2
                    }
                }
            };
            var sutBuilder = new SutBuilder();

            using (var context = new InMememoryOrderDbContextBuilder().Build())
            {
                var sut = sutBuilder.BuildSut(context);

                await context.AddAsync(stubProduct).ConfigureAwait(false);
                await context.AddAsync(stubProduct1).ConfigureAwait(false);
                await context.AddAsync(stubProduct2).ConfigureAwait(false);
                await context.AddAsync(stubOrder).ConfigureAwait(false);
                await context.AddAsync(stubOrder2).ConfigureAwait(false);
                await context.AddAsync(stubOrder3).ConfigureAwait(false);

                await context.SaveChangesAsync();

                var result = await sut.GetRecentOrderByCustomerIdAsync("1234", default).ConfigureAwait(false);

                result.Should().NotBeNull();
                result.OrderId.Should().Be(stubOrder3.OrderId);
                result.OrderItems.Count.Should().Be(2);
                result.OrderItems.Any(x => x.ProductId == 2).Should().BeTrue();
                result.OrderItems.Any(x => x.ProductId == 3).Should().BeTrue();
                result.OrderItems.Any(x => x.ProductId == 1).Should().BeFalse();
            }
        }

        private class SutBuilder
        {
            public OrderRepository BuildSut(OrderDbContext context)
            {
                return new OrderRepository(context);
            }
        }

    }
}
