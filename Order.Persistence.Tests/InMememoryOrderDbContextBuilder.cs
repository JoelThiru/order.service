﻿namespace Order.Persistence.Tests
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Order.Persistence;

    public class InMememoryOrderDbContextBuilder
    {
        public OrderDbContext Build(string dbName = null)
        {
            if (string.IsNullOrWhiteSpace(dbName))
            {
                dbName = Guid.NewGuid().ToString().Substring(0, 8);
            }

            var builder = new DbContextOptionsBuilder<OrderDbContext>();
            builder.UseInMemoryDatabase(dbName);
            var options = builder.Options;

            return new OrderDbContext(options);
        }
    }
}
