﻿namespace Order.Domain.Repositories
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IOrderRepository
    {
        Task<Order> GetRecentOrderByCustomerIdAsync(string customerId, CancellationToken cancellationToken);
    }
}
