﻿namespace Order.Persistence.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using Order.Domain.Repositories;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class OrderRepository : IOrderRepository
    {
        private readonly IDbContext dbContext;

        public OrderRepository(IDbContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<Domain.Order> GetRecentOrderByCustomerIdAsync(string customerId, CancellationToken cancellationToken)
        {
            return await this.dbContext.Set<Domain.Order>()
                .Where(x => x.CustomerId.Equals(customerId))
                .Include(x => x.OrderItems)
                .ThenInclude(X => X.Product)
                .OrderByDescending(x => x.OrderDate)
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}
