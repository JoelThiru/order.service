﻿
namespace Order.Persistence
{
    using Microsoft.EntityFrameworkCore;
    using System;

    public interface IDbContext : IDisposable
    {
        void Dispose();
        DbSet<T> Set<T>() where T : class;
    }
}
