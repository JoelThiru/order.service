﻿namespace Order.Persistence.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class OrderItemEntityConfiguration : IEntityTypeConfiguration<Order.Domain.OrderItem>
    {
        public void Configure(EntityTypeBuilder<Domain.OrderItem> builder)
        {
            builder.ToTable("OrderItems");
            builder.HasKey(cr => cr.OrderItemId);
            builder.Property(cr => cr.OrderId)
                .IsRequired();
            builder.Property(cr => cr.ProductId)
                .IsRequired();
            builder.Property(cr => cr.Quantity)
                .IsRequired();
            builder.Property(cr => cr.Price)
                .IsRequired();
            builder.Property(cr => cr.Returnable)
                .IsRequired();

            builder.HasOne(x => x.Order)
                .WithMany(x => x.OrderItems)
                .HasForeignKey(x => x.OrderId);
            builder.HasOne(x => x.Product)
                .WithMany()
                .HasForeignKey(x => x.ProductId);
        }
    }
}
