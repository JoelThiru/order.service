﻿namespace Order.Persistence.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class OrderEntityConfiguration : IEntityTypeConfiguration<Order.Domain.Order>
    {
        public void Configure(EntityTypeBuilder<Domain.Order> builder)
        {
            builder.ToTable("Orders");
            builder.HasKey(cr => cr.OrderId);
            builder.Property(cr => cr.CustomerId)
                .HasMaxLength(30)
                .IsRequired();
            builder.Property(cr => cr.OrderDate)
                .HasColumnType("Date")
                .IsRequired();
            builder.Property(cr => cr.DeliveryExpected)
                .HasColumnType("Date")
                .IsRequired();
            builder.Property(cr => cr.ContainsGift)
                .IsRequired();
            builder.Property(cr => cr.OrderSource)
                .HasMaxLength(30)
                .IsRequired();
            builder.Property(cr => cr.ShippingMode)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}
