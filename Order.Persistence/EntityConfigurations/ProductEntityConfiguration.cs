﻿namespace Order.Persistence.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductEntityConfiguration : IEntityTypeConfiguration<Order.Domain.Product>
    {
        public void Configure(EntityTypeBuilder<Domain.Product> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(cr => cr.ProductId);
            builder.Property(cr => cr.ProductName)
                .HasMaxLength(50)
                .IsRequired();
            builder.Property(cr => cr.PackHeight)
                .IsRequired();
            builder.Property(cr => cr.PackWeight)
                .IsRequired();
            builder.Property(cr => cr.PackWidth)
                .IsRequired();
            builder.Property(cr => cr.Colour)
                .HasMaxLength(20)
                .IsRequired();
            builder.Property(cr => cr.Size)
                .HasMaxLength(20)
                .IsRequired();
        }
    }
}
