﻿namespace Order.Persistence
{
    using Autofac;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Reflection;

    public class OrderPersistenceModule : Autofac.Module
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderPersistenceModule"/> class.
        /// </summary>
        /// <param name="configuration">The configuration <see cref="IConfiguration"/>.</param>
        public OrderPersistenceModule(
            IConfiguration configuration) => this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

        /// <summary>
        /// The Load.
        /// </summary>
        /// <param name="builder">The builder <see cref="ContainerBuilder"/>.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsImplementedInterfaces();

            var connectionString = this.configuration.GetSection("Persistence:SqlOptions:Connectionstring").Get<string>();

            builder.Register(c =>
            {
                var opt = new DbContextOptionsBuilder<OrderDbContext>();
                opt.UseSqlServer(connectionString);

                return new OrderDbContext(opt.Options);
            }).AsSelf()
            .AsImplementedInterfaces()
            .InstancePerLifetimeScope();
        }
    }
}
