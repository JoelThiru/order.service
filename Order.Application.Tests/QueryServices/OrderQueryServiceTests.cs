﻿namespace Order.Application.Tests.QueryServices
{
    using AutoFixture;
    using AutoFixture.AutoMoq;
    using AutoMapper;
    using FluentAssertions;
    using Moq;
    using Order.Application.Exceptions;
    using Order.Application.HttpServices;
    using Order.Application.QueryServices;
    using Order.Application.QueryServices.Dtos;
    using Order.Domain.Repositories;
    using System.Threading.Tasks;
    using Xunit;

    public class OrderQueryServiceTests
    {
        [Fact]
        public void Constructor_GaurdsAgainstNullArguments()
        {
            TestUtils.AssertConstructorsGuardAgainstNullArgs<OrderQueryService>();
        }

        [Fact]
        public async Task GetOrderReturnsNullIfCustomerNotFound()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "some user"
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockCustomerApiService.Setup(x => x.GetByEmailIdAsync(stubRequestModel.User, default))
                .ReturnsAsync((HttpServices.Dtos.Customer)null);

            var result = await sut.GetOrder(stubRequestModel, default).ConfigureAwait(false);

            result.Should().BeNull();
        }

        [Fact]
        public async Task GetOrderThrowsExceptionIfCustomerIdDoesNotMatch()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "some user",
                CustomerId = "customerId"
            };
            var stubCustomer = new HttpServices.Dtos.Customer
            {
                CustomerId = "differentCustomerId"
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockCustomerApiService.Setup(x => x.GetByEmailIdAsync(stubRequestModel.User, default))
                .ReturnsAsync(stubCustomer);

            sut.Invoking(x => x.GetOrder(stubRequestModel, default))
                .Should()
                .ThrowAsync<InvalidCustomerIdException>()
                .WithMessage("User's customerId does not match");
        }

        [Fact]
        public async Task GetOrderReturnOnlyCustomerIfNoOrderFound()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "some user",
                CustomerId = "customerId"
            };
            var stubCustomer = new HttpServices.Dtos.Customer
            {
                CustomerId = "customerId",
                FirstName = "firstname",
                LastName = "lastName",
                HouseNumber = "1a",
                Street = "street",
                Town = "town",
                Postcode = "postCode"
            };
            var stubDtoCustomer = new Order.Application.QueryServices.Dtos.Customer
            {
                FirstName = stubCustomer.FirstName,
                LastName = stubCustomer.LastName
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockCustomerApiService.Setup(x => x.GetByEmailIdAsync(stubRequestModel.User, default))
                .ReturnsAsync(stubCustomer);
            sutBuilder.MockOrderRepository.Setup(x => x.GetRecentOrderByCustomerIdAsync(stubRequestModel.CustomerId, default))
                .ReturnsAsync((Domain.Order)null);
            sutBuilder.MockMapper.Setup(x => x.Map<Order.Application.QueryServices.Dtos.Customer>(stubCustomer))
                .Returns(stubDtoCustomer);

            var result = await sut.GetOrder(stubRequestModel, default).ConfigureAwait(false);

            result.Customer.Should().NotBeNull();
            result.Customer.FirstName.Should().Be(stubCustomer.FirstName);
            result.Customer.LastName.Should().Be(stubCustomer.LastName);
            result.Order.Should().BeNull();
        }

        [Fact]
        public async Task GetOrderReturnVaildCustomerWithOrder()
        {
            var stubRequestModel = new GetOrderRequestModel
            {
                User = "some user",
                CustomerId = "customerId"
            };
            var stubCustomer = new HttpServices.Dtos.Customer
            {
                CustomerId = "customerId",
                FirstName = "firstname",
                LastName = "lastName",
                HouseNumber = "1a",
                Street = "street",
                Town = "town",
                Postcode = "postCode"
            };
            var stubOrder = new Domain.Order
            {
                OrderId = 1234
            };
            var stubDtoCustomer = new Order.Application.QueryServices.Dtos.Customer
            {
                FirstName = stubCustomer.FirstName,
                LastName = stubCustomer.LastName
            };
            var stubDtoOrder = new Order.Application.QueryServices.Dtos.Order
            {
                OrderNumber = 1234
            };
            var sutBuilder = new SutBuilder();
            var sut = sutBuilder.Build;

            sutBuilder.MockCustomerApiService.Setup(x => x.GetByEmailIdAsync(stubRequestModel.User, default))
                .ReturnsAsync(stubCustomer);
            sutBuilder.MockOrderRepository.Setup(x => x.GetRecentOrderByCustomerIdAsync(stubRequestModel.CustomerId, default))
                .ReturnsAsync(stubOrder);
            sutBuilder.MockMapper.Setup(x => x.Map<Order.Application.QueryServices.Dtos.Customer>(stubCustomer))
                .Returns(stubDtoCustomer);
            sutBuilder.MockMapper.Setup(x => x.Map<Order.Application.QueryServices.Dtos.Order>(stubOrder))
                .Returns(stubDtoOrder);

            var result = await sut.GetOrder(stubRequestModel, default).ConfigureAwait(false);

            result.Customer.Should().NotBeNull();
            result.Customer.FirstName.Should().Be(stubCustomer.FirstName);
            result.Customer.LastName.Should().Be(stubCustomer.LastName);
            result.Order.Should().NotBeNull();
            result.Order.OrderNumber.Should().Be(stubOrder.OrderId);
            result.Order.DeliveryAddress.Should().Be(stubCustomer.DeliveryAddress);
        }

        private class SutBuilder
        {
            private readonly Fixture fixture;

            public Mock<IOrderRepository> MockOrderRepository { get; }
            public Mock<ICustomerApiService> MockCustomerApiService { get; }
            public Mock<IMapper> MockMapper { get; }

            public SutBuilder()
            {
                this.fixture = new Fixture();
                this.fixture.Customize(new AutoMoqCustomization());

                this.MockCustomerApiService = this.fixture.Freeze<Mock<ICustomerApiService>>();
                this.MockOrderRepository = this.fixture.Freeze<Mock<IOrderRepository>>();
                this.MockMapper = this.fixture.Freeze<Mock<IMapper>>();
            }

            public OrderQueryService Build
                => new OrderQueryService(
                    this.MockOrderRepository.Object,
                    this.MockCustomerApiService.Object,
                    this.MockMapper.Object);

        }
    }
}
