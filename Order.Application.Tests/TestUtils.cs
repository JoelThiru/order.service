﻿namespace Order.Application.Tests
{
    using AutoFixture;
    using AutoFixture.AutoMoq;
    using AutoFixture.Idioms;
    using FluentAssertions.Execution;

    public static class TestUtils
    {
        /// <summary>
        /// The AssertConstructorsGuardAgainstNullArgs.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        public static void AssertConstructorsGuardAgainstNullArgs<T>()
        {
            using (new AssertionScope())
            {
                var fixture = new Fixture();
                fixture.Customize(new AutoMoqCustomization());
                var assertion = new GuardClauseAssertion(fixture);
                var constructors = typeof(T).GetConstructors();
                assertion.Verify(constructors);
            }
        }
    }
}
