﻿namespace Order.Application.Tests.HttpServices
{
    using Order.Application.HttpServices;
    using Xunit;

    public class CustomerApiServiceTests
    {
        [Fact]
        public void Constructor_GaurdsAgainstNullArguments()
        {
            TestUtils.AssertConstructorsGuardAgainstNullArgs<CustomerApiService>();
        }
    }
}
